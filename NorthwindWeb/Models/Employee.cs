﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindWeb
{
    public partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }
}